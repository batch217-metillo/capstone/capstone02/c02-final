const User = require("../models/userModel.js");
const Product = require("../models/productModel.js");
const auth = require("../auth.js");
const product = require("../routes/productRoutes.js");

// Create Product
/*
@desc This is for admin role only. Non-Admin/User cannot add products on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.addProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			category: data.product.category,
			description: data.product.description,
			stocks: data.product.stocks,
			price: data.product.price
		})

		return newProduct.save().then((newProduct, error) => {
			if(error) {
				return false
			}else {
				return `Product has been added to the database!`
			}
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

			You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Get All Products (active or inactive)
/*
@desc This is for admin role only. Admin role can view all products, active or inactive. Non-Admin/User cannot view inactive products on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getAllProducts = (data) => {
	if(data.isAdmin){
		return Product.find({}).then(result => {
		return result
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Get All ACTIVE Product 
/*
@desc All Users (Admin and Non-Admin Role) can view all active products on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// Get Single Product (productId)
/*
@desc All Users (Admin and Non-Admin) can view a specific product (active or inactive) using its id
@param {JSON Object}
@return {String} different messages
*/
module.exports.getSpecificProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result
	})
}

// Below codes are not working
// Get Single Product (productName)
/*
@desc All Users (Admin and Non-Admin) can view a specific product (active or inactive) using its name
@param {JSON Object}
@return {String} different messages
*/
/*module.exports.getSpecificProductUsingProductName = (productName) => {
	return Product.find({productName}).then(result => {
		return result
	})
}*/

// Update a Product
/*
@desc This is for admin role only. Admin role can update product information.
@param {JSON Object}
@return {String} different messages
*/
module.exports.updateProduct = (userData, productId, newData) => {
	if(userData.isAdmin) {
		return Product.findByIdAndUpdate(
			productId, {
				name: newData.name,
				category: newData.category,
				description: newData.description,
				stocks: newData.stocks,
				price: newData.price,
				isActive: newData.isActive
		})
		.then((result, error) => {
			if(error){
				return false
			}

			result.name = newData.name;
			result.category = newData.category;
			result.description = newData.category;
			result.stocks = newData.stocks;
			result.price = newData.price;
			result.isActive = newData.isActive;
			return result.save().then((updatedProduct, saveError) => {
				if(saveError) {
					return false;
				}else {
					return updatedProduct;
				}
			})
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Archive a Product (using productId only)
/*
@desc This is for admin role only. Admin role can archive a product using its id and automatically change its isActive status to false.
@param {JSON Object}
@return {String} different messages
*/
module.exports.archiveProduct = (user, productId) => {
	if(user.isAdmin) {
		return Product.findByIdAndUpdate( productId, {
			isActive: false
		})
		.then((result, error) => {
			if(error) {
				return false
			}

			return result.save().then((archivedProduct, saveErr) => {
				if(saveErr) {
					return false;
				}else {
					return `You have successfully updated the product status to not active.`
				}
			})
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Archiving a product (all users)
/*module.exports.archiveProduct = (productId) => {
		return Product.findByIdAndUpdate( productId, {
			isActive: false
		})
		.then((result, error) => {
			if(error) {
				return false
			}

			result.isActive = result.isActive;
			return result.save().then((archivedProduct, saveErr) => {
				if(saveErr) {
					return false;
				}else {
					return `You have successfully updated the product status to not active.`
				}
			})
		})
}*/

// Deleting a Product (using productId only)
/*
@desc This is for admin role only. Admin role can delete a product using its id.
@param {JSON Object}
@return {String} different messages
*/
module.exports.deleteProduct = (token, productId) => {
	if(token.isAdmin) {
		return Product.findByIdAndDelete( productId)
		.then((result) => {
			if(result) {
				return `You have successfully deleted a product on the database.`
			}else {
				return `Product ID does not exist.`
			}
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}