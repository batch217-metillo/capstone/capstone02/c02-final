const User = require("../models/userModel.js");
const Product = require("../models/productModel.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// Get All Users 
/*
@desc This is for admin role only. Admin role can view all users registered on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getAllUsers = (data) => {
	if(data.isAdmin){
		return User.find({}).then(result => {
		return result
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Get Single User - all information
/*
@desc This is for admin role only. Admin role can view all information of a single user that has registered on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getUserProfile = (data, userId) => {
	if(data.isAdmin){
		return User.findById(userId).then(result => {
			return result
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to perform this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Get Single User - filtered information
/*
@desc Admin role or non-admin role can view single user's filtered information that has registered on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}

/*module.exports.getProfile = (userId) => {
	return User.findById(userId)
	.then((response) => response.json())
	.then((json) => {
		let info = json.map((userInfo => {
			return {
				firstName: userInfo.firstName,
				lastName: userInfo.lastName,
				emailAddress: userInfo.emailAddress,
				phoneNumber: userInfo.phoneNumber
			}
		}))
	})		
}*/
// ^ Not Working

// Set a user as admin
/*
@desc This is for admin role only. Admin role can set a non-admin role user to admin role.
@param {JSON Object}
@return {String} different messages
*/
module.exports.setAsAdmin = (user, userId) => {
	return User.findByIdAndUpdate(
		userId, {
			isAdmin: true
		})
		.then((updatedRole, error) => {
			if(error) {
				return false
			}else {
				return `Non-admin user is now an Admin.`
			}
		})
}

// Set an admin as user
/*
@desc This is for admin role only. Admin role can set an admin role back to user role.
@param {JSON Object}
@return {String} different messages
*/
module.exports.setAsUser = (user, userId) => {
	return User.findByIdAndUpdate(
		userId, {
			isAdmin: false
		})
		.then((updatedRole, error) => {
			if(error) {
				return false
			}else {
				return `Successfull updated the role as a user.`
			}
		})
}

// Deleting a user
/*
@desc 
@param 
@return
*/
/*module.exports.deleteProduct = (data, userId) => {
	if(data.userId == req.params.userId) {
		users.splice(users[i], 1);
				message = `User ${req.body.username} has been deleted`;
				break;
	}*/
// ^not working

// Add to cart
/*
@desc 
@param 
@return
*/
module.exports.addToCart = async (data) => {
	if(!data.isAdmin) {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orders.push({productId: data.productId, 
				quantity: data.quantity});

			return user.save().then((user, error) => {
				if(error) {
					return false;
				}else {
					return true
				}
			})
		})

		let isOrderUpdated = await Product.findById(data.productId).then(product => {
			product.orders.push({ userId: data.userId});

			return product.save().then((product, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		})

		if(isUserUpdated && isOrderUpdated) {
			return `Successfully added to your cart.`;
		}else {
			return false
		}
	}else {
		let message = Promise.resolve(`You are not allowed to perform this task.`)
		return message.then((value) => {
			return value
		})
	}
}
		

/*module.exports.addToCart = async (data) => {
	if(data.isAdmin) {
		return `You are not allowed to perform this task.`
	}else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orders.push({productId: data.productId, quantity: data.quantity});

			return user.save().then((user, error) => {
				if(error) {
					return false;
				}else {
					return true
				}
			})
		})

		let isOrderUpdated = await Product.findById(data.productId).then(product => {
			product.orders.push({orderId: data.orderId, userId: data.userId});

			return product.save().then((product, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		})

		if(isUserUpdated && isOrderUpdated) {
			return true;
		}else {
			return false
		}
	}
}*/