const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required!"]
	},
	category: {
		type: String,
		required: [true, "Product category is required!"]
	},
	description: {
		type: String,
		required: [true, "Product description is required!"]
	},
	stocks: {
		type: Number,
		required: [true, "Stock number is required!"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [{
		/*orderId: {
			type: String,
			required: [true, "Order Id is required!"]
		},*/
		userId: {
			type: String,
			required: [true, "User Id is required!"]
		},
		orderCreatedOn: {
			type: Date,
			default: new Date()
		}
	}]
	/*,
	orders: [{
		orderId: {
			type: String,
			required: [true, "Order Id is required!"]
		},
		userId: {
			type: String,
			required: [true, "User Id is required"]
		},
		totalPrice: {
			type: Number,
			required: [true, "Total Price is required"]
		},
		quantity: {
			type: Number,
			required: [true, "Quantity is required"]
		},
		orderCreatedOn: {
			type: Date,
			default: new Date()
		}
	}]*/
})

module.exports = mongoose.model("Product", productSchema)