const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	emailAddress: {
		type: String,
		required: [true, "Email Address is required!"]
	},
	phoneNumber: {
		type: String,
		required: [true, "Phone Number is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [{
		products: [{
			productId: {
			type: String,
			required: [true, "Product Id is required!"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity field is required!"]
			},
			_id: false,
		}],
		/*totalAmount: {
			type: Number,
			required: [true, "Total amount field is required!"]
		},*/
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
	/*address: [{
		street: {
			type: String,
			required: [true, "Street name is required!"]
		},
		barangay: {
			type: String,
			required: [true, "Barangay name is required!"]
		},
		city: {
			type: String,
			required: [true, "City name is required!"]
		},
		zipCode: {
			type: Number,
			required: [true, "Zip code number is required!"]
		},
		addressSettings: [{
			isHomeAddress: {
				type: Boolean,
				default: true
			},
			isDefaultAddress: {
				type: Boolean,
				default: true
			}
		}]
	}],*/
/*	orders: [{
		products: [{
			productId: {
			type: String,
			required: [true, "Product Id is required!"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity field is required!"]
			}
		}],
		totalAmount: {
			type: Number,
			required: [true, "Total amount field is required!"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]*/
})

module.exports = mongoose.model("User", userSchema);