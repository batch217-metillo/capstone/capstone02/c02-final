/*
	Entry Point for Registration and Authentication
*/

const express = require("express");
const router = express.Router();
const User = require("../models/userModel.js");
const entryControllers = require("../controllers/entryControllers.js");
const auth = require("../auth.js");

// Checking Email
router.post("/checkEmail", (req, res) => {
	entryControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// User Registration
router.post("/register", (req, res) => {
	entryControllers.userRegistration(req.body).then(resultFromController => res.send(resultFromController));
})

// User Login
router.post("/login", (req, res) => {
	entryControllers.userLogin(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;