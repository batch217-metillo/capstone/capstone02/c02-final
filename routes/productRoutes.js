const express = require("express");
const router = express.Router();
const Product = require("../models/productModel.js");
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// Create Product [Admin role only]
router.post("/add", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.addProduct(data).then(resultFromController => { 
		res.send(resultFromController)
	})
})

// Retrieve all products (active and inactive) [Admin role Only]
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	productControllers.getAllProducts(data).then(resultFromController => {res.send(resultFromController)
	})
})

// Retrieve all active products
router.get("/allActive", (req, res) => {
	productControllers.getAllActiveProducts().then(resultFromController => {
		res.send(resultFromController)
	})
})

// Retrieve single product (Using productId url parameter)
router.get("/:productId", (req, res) => {
	productControllers.getSpecificProduct(req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Below codes are not working
// Retrieve single products (Using productName url parameter)
/*router.get("/:productName", (req, res) => {
	productControllers.getSpecificProductUsingProductName(req.params.productName).then(resultFromController => {
		res.send(resultFromController)
	})
})*/

// Update Product Information
router.patch("/:productId/update", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	productControllers.updateProduct(userData, req.params.productId, req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})


// Archive a Product (all users)
/*router.patch("/:productId/archive", auth.verify, (req, res) => {
	productControllers.archiveProduct(req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})*/

// Archive a Product (admin only)
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	productControllers.archiveProduct(user, req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;

// Deleting a Product
router.delete("/:productId/delete", auth.verify, (req, res) => {
	const token = auth.decode(req.headers.authorization);

	productControllers.deleteProduct(token, req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})