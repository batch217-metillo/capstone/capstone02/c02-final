const express = require("express");
const router = express.Router();
const User = require("../models/userModel.js");
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Get all user (admin only)
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	userControllers.getAllUsers(data).then(resultFromController => {res.send(resultFromController)
	})
})

// Get single user - all information (admin only)
router.get("/:userId/profile", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.getUserProfile(data, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Get single user - filtered information
router.get("/:userId", (req, res) => {
	userControllers.getProfile(req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Set a user as admin (admin only)
router.patch("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAsAdmin(user, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Set an admin as user (admin only)
router.patch("/:userId/setAsUser", auth.verify, (req, res) => {
	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAsUser(user, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Deleting a User
/*router.delete("/:userId/delete", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		emailAddress: auth.decode(req.headers.authorization).emailAddress,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.deleteUser(data, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})*/
// ^not working


// Add to cart
router.post("/addToCart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	userControllers.addToCart(data)
	.then(resultFromController => {
	res.send(resultFromController)
	})
})


module.exports = router;